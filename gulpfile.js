var $, gulp, manifest, mergeStream, runSequence, shelljs;

gulp = require('gulp');

shelljs = require('shelljs');

mergeStream = require('merge-stream');

runSequence = require('run-sequence');

manifest = require('./package.json');

$ = require('gulp-load-plugins')();

gulp.task('clean', function() {
    shelljs.rm('-rf', './build');
    return shelljs.rm('-rf', './dist');
});
// ['win32', 'osx64', 'linux32', 'linux64']
['win32'].forEach(function(platform) {
    return gulp.task('build:' + platform, function() {
        if (process.argv.indexOf('--toolbar') > 0) {
            shelljs.sed('-i', '"toolbar": false', '"toolbar": true', './src/package.json');
        }
        return gulp.src('./src/**').pipe($.nodeWebkitBuilder({
            platforms: [platform],
            version: '0.12.2',
            winIco: process.argv.indexOf('--noicon') > 0 ? void 0 : './assets-windows/icon.ico',
            macIcns: './assets-osx/icon.icns',
            macZip: true,
            macPlist: {
                NSHumanReadableCopyright: 'aluxian.com',
                CFBundleIdentifier: 'com.aluxian.starter'
            }
        })).on('end', function() {
            if (process.argv.indexOf('--toolbar') > 0) {
                return shelljs.sed('-i', '"toolbar": true', '"toolbar": false', './src/package.json');
            }
        });
    });
});

gulp.task('sign:osx64', ['build:osx64'], function() {
    shelljs.exec('codesign -v -f -s "Alexandru Rosianu Apps" ./build/Starter/osx64/Starter.app/Contents/Frameworks/*');
    shelljs.exec('codesign -v -f -s "Alexandru Rosianu Apps" ./build/Starter/osx64/Starter.app');
    shelljs.exec('codesign -v --display ./build/Starter/osx64/Starter.app');
    return shelljs.exec('codesign -v --verify ./build/Starter/osx64/Starter.app');
});

gulp.task('pack:osx64', ['sign:osx64'], function() {
    shelljs.mkdir('-p', './dist');
    shelljs.rm('-f', './dist/Starter.dmg');
    return gulp.src([]).pipe(require('gulp-appdmg')({
        source: './assets-osx/dmg.json',
        target: './dist/Starter.dmg'
    }));
});

gulp.task('pack:win32', ['build:win32'], function() {
    return shelljs.exec('makensis ./assets-windows/installer.nsi');
});

[32, 64].forEach(function(arch) {
    return ['deb', 'rpm'].forEach(function(target) {
        return gulp.task("pack:linux" + arch + ":" + target, ['build:linux' + arch], function() {
            var move_opt, move_png256, move_png48, move_svg;
            shelljs.rm('-rf', './build/linux');
            move_opt = gulp.src(['./assets-linux/starter.desktop', './assets-linux/after-install.sh', './assets-linux/after-remove.sh', './build/Starter/linux' + arch + '/**']).pipe(gulp.dest('./build/linux/opt/starter'));
            move_png48 = gulp.src('./assets-linux/icons/48/starter.png').pipe(gulp.dest('./build/linux/usr/share/icons/hicolor/48x48/apps'));
            move_png256 = gulp.src('./assets-linux/icons/256/starter.png').pipe(gulp.dest('./build/linux/usr/share/icons/hicolor/256x256/apps'));
            move_svg = gulp.src('./assets-linux/icons/scalable/starter.png').pipe(gulp.dest('./build/linux/usr/share/icons/hicolor/scalable/apps'));
            return mergeStream(move_opt, move_png48, move_png256, move_svg).on('end', function() {
                var output, port;
                shelljs.cd('./build/linux');
                port = arch === 32 ? 'i386' : 'x86_64';
                output = "../../dist/Starter_linux" + arch + "." + target;
                shelljs.mkdir('-p', '../../dist');
                shelljs.rm('-f', output);
                shelljs.exec("fpm -s dir -t " + target + " -a " + port + " --rpm-os linux -n starter --after-install ./opt/starter/after-install.sh --after-remove ./opt/starter/after-remove.sh --license MIT --category Chat --url \"https://example.com\" --description \"A sample NW.js app.\" -m \"Alexandru Rosianu <me@aluxian.com>\" -p " + output + " -v " + manifest.version + " .");
                return shelljs.cd('../..');
            });
        });
    });
});

gulp.task('pack:all', function(callback) {
    return runSequence('pack:osx64', 'pack:win32', 'pack:linux32:deb', 'pack:linux64:deb', callback);
});

gulp.task('run:osx64', ['build:osx64'], function() {
    return shelljs.exec('open ./build/Starter/osx64/Starter.app');
});

gulp.task('open:osx64', function() {
    return shelljs.exec('open ./build/Starter/osx64/Starter.app');
});

gulp.task('release', ['pack:all'], function(callback) {
    return gulp.src('./dist/*').pipe($.githubRelease({
        draft: true,
        manifest: manifest
    }));
});

gulp.task('default', ['pack:win32']);